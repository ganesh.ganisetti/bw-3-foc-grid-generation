clc;close all;clear all;
addpath('./scripts');

%% Inputs and Load data
bw3filenames = {'BW3_20240528_2310_CDGPS_TEST_AST_HanaIceCreamHI_HIC_01 to Satellite.csv'};    % Predicted CDGPS log file
str_name = {'IC-098'};
shifted_grid = [];
for bw = 1:length(bw3filenames)
    bw3filename = bw3filenames{bw};
    fc = 894e6; % for uplink analysis
    RollError = 2.5;    % Roll error in degrees that is applied on the array
    Deformation = 0;    % To enable or disable deformation
    
    % Select beamforming microns (as per BW3's micron IDs): 48 micron array is the base for UL
    micron_nw_idx = [100,101,102,103,116,118,119,129,130,131,132,133,144,145,146,147,156,157,158,159,160,161];
    micron_ne_idx = [97,109:111,120,121,123,124,125,134,135,136,138,139,149,150,151,152,153,162,163,165,166,167,176,177,178,190,191,192,193];
    micron_sw_idx = [7,20,21,30,31,32,33,34,35,44,45,46,47,48,49,59,60,61,62,63,72,73,74,75,76,86,87,88,89,90];
    micron_idx = [micron_ne_idx micron_nw_idx micron_sw_idx];
    
    if contains(str_name{bw},'MID')
        % Midland test location
        elat = 31.931;	
        elong = -102.208;	
        beam_alt = 0.848;
        
        test_site_str = 'MIDLAND';
        
    elseif contains(str_name{bw},'KAP')

        % Kapolei test location
        elat = 21.337;
        elong = -158.09015;
        beam_alt = 0.051;

        test_site_str = 'KAPOLEI';
    elseif contains(str_name{bw},'IC')

        % Icecream test location
        elat = 20.7987;	
        elong = -156.0669;
        beam_alt = 0.221;

        test_site_str = 'ICE CREAM';
        
    elseif contains(str_name{bw},'RAK') && contains(str_name{bw},'-N')
        % North Hokkaido test location
        elat = 45.0936;
        elong = 141.9567;        
        beam_alt = 2.290;
        
        test_site_str = 'North Hokkaido';
        
    elseif contains(str_name{bw},'RAK') && contains(str_name{bw},'-S')
        % South Hokkaido test location
        elat = 42.6844;
        elong = 141.9836;
        beam_alt = 2.290;
        
        test_site_str = 'South Hokkaido';
        
    elseif contains(str_name{bw},'KEN')
        
        if contains(str_name{bw},'-GW')
            elat = -1.0299;
            elong = 37.0737;
            beam_alt = 1.550;
        else
            elat = -2.9592;
            elong = 38.8146;
            beam_alt = 0.396;
        end
        test_site_str = 'Tsavo Park';
    end
    %% Simulation parameters
    Re = 6378.135;      % Radius of earth
    c =  299792458;         % speed of light in m/s
    TotalBeams = 10;
    step_size = 20;
    
    % Create a grid (~18km from test location) around the test location
    LLA1 = Ten_Beam_Placement_func(elat,elong,elat,elong,beam_alt);
    LLA2 = round(LLA1,4);
    elatarr = LLA2(:,1);
    elongarr = LLA2(:,2);
    
    data = xlsread(bw3filename);
    u=1;
    lat = data(1:TotalBeams:end,u);
    long = data(1:TotalBeams:end,u+1);
    altitude = data(1:TotalBeams:end,u+2);
    rLL = data(1:TotalBeams:end,u+3)*180/pi;
    pch = data(1:TotalBeams:end,u+4)*180/pi;
    yw = data(1:TotalBeams:end,u+5)*180/pi;
    
    Ro = Re + altitude;
    constellation = 'polar';
    
    groundAntennaGain = 25; % in dB
    Input_pwr = -10; % Desired power level of input signal in dB
    
    signal_type = 0;    % 0 -> CW; 1 -> LTE (10MHz BW)
    
    %% Gain contour setup
    sel = 2;                % Size of Grid (+sel� to -sel� in latitude and +sel� to -sel� in longitude)
    gc_res = 0.02;          % Resolution of grid (smaller resolutions are more accurate but more time/memory consuming)
    % 3D plot
    xs = Ro.*cosd(lat).*cosd(long);
    ys = Ro.*cosd(lat).*sind(long);
    zs = Ro.*sind(lat);
    
    xe = Re.*cosd(elatarr).*cosd(elongarr);
    ye = Re.*cosd(elatarr).*sind(elongarr);
    ze = Re.*sind(elatarr);
    
    h = figure('units','normalized','outerposition',[0 0 1 1]);subplot(1,2,1);hold on;
    earth_sphere('km');
    ax = gca; ax.Clipping = 'off';    % turn clipping off
    axis off;
    zoom(8);
    hold on;plot3(xs,ys,zs,'yx');
    hold on;plot3(xe,ye,ze,'kx','Markersize',2);
    
    axlen=1000;
    axes=['sx';'sy';'sz'];
    colour=['r','g','b'];
    scale_array = 100;
    
    % 2D
    subplot(1,2,2);box on;
    axis([-180 180 -90 90])
    load('topo.mat', 'topo', 'topomap1');
    topoe = [topo(:,181:360) topo(:,1:180)];
    contour(-179:180, -89:90, topoe, [0 0], 'k','HandleVisibility','off')
    hold on;h11 = plot(long,lat,'yx','MarkerSize',8,'Displayname','Satellite track');
    
    for j = 1:10
        hold on;h12 = plot(elongarr(j),elatarr(j),'kx','MarkerSize',18,'HandleVisibility','off');
        hold on;t11 = text(elongarr(j)-0.05,elatarr(j),num2str(j),'FontSize',12,'HandleVisibility','off');
    end
    axis([elongarr(1)-sel elongarr(1)+sel  elatarr(1)-sel elatarr(1)+sel]);
    xlabel('Longitude (\circ)');ylabel('Latitude (\circ)');
    
    save_anim = 1;
    if bw == 1 && save_anim == 1
        writerObj = VideoWriter('Video2_anim.mp4','MPEG-4'); %// initialize the VideoWriter object
        writerObj.FrameRate = 1;
        open(writerObj);
    end
    
    %% Create BW3 Array
    dx = 0.161;             % spacing between elements in meters; spacing selected for 960 MHz operation
    dy = 0.161;             % spacing between elements in meters; spacing selected for 960 MHz operation
    skip_element = 0;
    hole = 1;
    maxNx = 56;
    maxNy = 56;
    configuration = 1;      % Complete BW3 array or BW3 small version
    [Toff,hole_ind] = create_BW3_microns(maxNx,maxNy,dx,dy,skip_element,hole,configuration);
    Toff(:,hole_ind == 1) = [];
    
    if Deformation == 1
        load('bfVectorsRelAocsFrame_2023_12_17.mat', 'bfVectors');
        deformed_Toff = bfVectors;
    else
        deformed_Toff = Toff;
    end
    
    iMicronMap = [4, 5, 6, 7, 8, 9, 10, 11, 18, 19, 20, 21, 22, 23, 24, 25, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 86, 87, 88, 89, 90, 93, 94, 95, 96, 97, 100, 101, 102, 103, 104, 107, 108, 109, 110, 111, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 172, 173, 174, 175, 176, 177, 178, 179, 186, 187, 188, 189, 190, 191, 192, 193];
    for i = 1:length(micron_idx)
        BF_microns_idx(i) = find(iMicronMap == micron_idx(i));
    end
    temp = (BF_microns_idx - 1)*16 + (1:16).';
    BF_elements = reshape(temp,1,16*length(BF_microns_idx));
%     figure;plot(Toff(2,:),Toff(1,:),'r.');hold on;plot(Toff(2,BF_elements),Toff(1,BF_elements),'c.');
    %% Input Signal
    sf_cell_count = 1;        % Number of signals
    if signal_type == 0
        fs  = 10e6;               % sampling frequency
        NumSubbands = 1000;       % for 10kHz resolution
        Nsamples = 2*NumSubbands; % Nsamples should be integer multiple of NumSubbands
        inputSig_beam=gen_cw(Nsamples,fs,0,sf_cell_count,0);
        inputSig_beam = inputSig_beam.';
    else
        load('LTESig_tm1p1_bw10mhz.mat', 'inputSig_beam')
        inputSig_beam = inputSig_beam(:,1:sf_cell_count);
    end
    
    txSigPwr = (1/size(inputSig_beam,1))*(sum(abs(inputSig_beam).^2,1));
    txSigPwr_dB = 10*log10(txSigPwr);
    
    Gain = Input_pwr*ones(length(txSigPwr_dB),1) - txSigPwr_dB;
    transmitter = sqrt(10.^(0.1*Gain));
    inputSig_beam = inputSig_beam .* transmitter;
    txSigPwr = (1/size(inputSig_beam,1))*(sum(abs(inputSig_beam).^2,1));
    txSigPwr_dB = 10*log10(txSigPwr);
    
    %% Beamforming
    RxPwr = [];
    cnt = 0;
    El_gnd_all = zeros(length(lat),1);
    for j = 1:length(lat)
        [El,Az,range]=sat2earthAZEL_elliptical(elatarr(1),elongarr(1),lat(j),long(j),Re,Ro(j),constellation,0,beam_alt);        
        alpha = acosd(cosd(El)*cosd(Az));
        El_gnd_all(j) = acosd((Ro(j) .* sind(alpha))./Re);
    end
    [max_El_gnd_all,max_El_gnd_all_idx] = max(El_gnd_all);
    
    pass = 1:step_size:length(lat);
    pass = [pass(pass<max_El_gnd_all_idx) max_El_gnd_all_idx pass(pass>max_El_gnd_all_idx)];
    
    mispointed_long = zeros(length(pass),1);
    mispointed_lat = zeros(length(pass),1);
    mispointed_dist = zeros(length(pass),1);
    lambda = 3e8/fc;
    for j=pass
        
        cnt = cnt + 1
        
        [phy_El,phy_Az,range]=sat2earthAZEL_elliptical(elatarr(1),elongarr(1),lat(j),long(j),Re,Ro(j),constellation,0,beam_alt);
        phy_dir_cos = [sind(phy_El);cosd(phy_El)*sind(phy_Az);cosd(phy_El)*cosd(phy_Az)];
        Tx_wave_no = 2*pi*fc/c;
        
        bw3_RPY_rotation = transpose(RotZMat(yw(j)*pi/180)*RotYMat(pch(j)*pi/180)*RotXMat(rLL(j)*pi/180));
        correction_rotation = RotXMat(RollError*pi/180);
        
        phy_pos =  bw3_RPY_rotation*correction_rotation*deformed_Toff;
        phy_Dist = (phy_pos(:,BF_elements))'*phy_dir_cos;
        phy_wts = exp(1i*Tx_wave_no*phy_Dist);
        
        ElementSignals = phy_wts*(inputSig_beam(:,1).');
        
        comp_pos = bw3_RPY_rotation*Toff;
        
        %% Gain contour
        elat_vec = [elatarr(1)-sel:gc_res:elatarr(1)+sel];    % Latitude range
        elong_vec = [elongarr(1)-sel:gc_res:elongarr(1)+sel] ; % Longitude range
        [Longmat,Latmat] = meshgrid(elong_vec,elat_vec);
        latlong = [Longmat(:)' ;Latmat(:)'];
        
        comp_Dist_GC = zeros(length(BF_elements),length(latlong));
        for id1=1:length(latlong)  % Latitude/Longitude to Azimuth/Elevation conversion
            [El_GC,Az_GC,~] = sat2earthAZEL_elliptical(latlong(2,id1),latlong(1,id1),lat(j),long(j),Re,Ro(j),constellation,0,beam_alt);
            comp_Dist_GC(:,id1) = (comp_pos(:,BF_elements))'*[sind(El_GC);cosd(El_GC).*sind(Az_GC);cosd(El_GC).*cosd(Az_GC)];
        end
        comp_wts_GC = exp(1i*Tx_wave_no*comp_Dist_GC);  % Computed weights
        
        Sat_RxSig_GC = ElementSignals.' * conj(comp_wts_GC);
        Sat_RxSigPwr_GC = (1/size(Sat_RxSig_GC,1))*(sum(abs(Sat_RxSig_GC).^2,1));
        Sat_RxSigPwrdB = 10*log10(Sat_RxSigPwr_GC);
        Sat_RxSigPwrdB_mat = reshape(Sat_RxSigPwrdB,length(elat_vec),length(elong_vec));
        DLNormSigpwrdB = Sat_RxSigPwrdB_mat-max(max(Sat_RxSigPwrdB_mat));
        
        [peak_col,peak_col_idx] = max(DLNormSigpwrdB);
        [peak_row,peak_row_idx] = max(peak_col);
        mispointed_lat(cnt) = Latmat(peak_col_idx(peak_row_idx),peak_row_idx);
        mispointed_long(cnt) = Longmat(peak_col_idx(peak_row_idx),peak_row_idx);
        
        lat1 = elatarr(1);
        long1 = elongarr(1);
        lat2 = mispointed_lat(cnt);
        long2 = mispointed_long(cnt);
        temp = (sin(lat1*pi/180) .* sin(lat2*pi/180)) + (cos(lat1*pi/180) .* cos(lat2*pi/180) .* cos(long2*pi/180 - long1*pi/180));
        mispointed_dist(cnt) = Re * acosd(temp)*pi/180;
        
        %% Visualization
        % Plot -3dB gain contours
        subplot(1,2,2);isogain = [-3:1:0];   % Desired gain contour levels
        [cs1,h4] = contour(elong_vec,elat_vec,DLNormSigpwrdB,isogain,'b');
        clabel(cs1,h4,'FontSize',11,'color',[1 0 0]);
        h4.LineWidth = 1;
        hold on;pp = plot(mispointed_long(cnt),mispointed_lat(cnt),'rx','Markersize',8);
        
        ECEF_rotation = RotYMat(-90*pi/180)*RotXMat(long(j)*pi/180)*RotYMat(-lat(j)*pi/180);
        
        % Visualization array rotation (First we rotate to NED, apply track RPY and then correction (Mathematically, we do correction first, RPY second, then NED rotation) .
        move_Toff = ECEF_rotation*bw3_RPY_rotation*correction_rotation* (Toff*scale_array);
        move_Toff2 = ECEF_rotation*bw3_RPY_rotation* (Toff*scale_array);
        
        axis_ECEF = ECEF_rotation*transpose(RotZMat(yw(j)*pi/180)*RotYMat(pch(j)*pi/180)*RotXMat(rLL(j)*pi/180))*correction_rotation;
        move_Toff = move_Toff + [xs(j);ys(j);zs(j)];
        move_Toff2 = move_Toff2 + [xs(j);ys(j);zs(j)];
        
        subplot(1,2,1);
        hold on;p4 = plot3(move_Toff(1,:),move_Toff(2,:),move_Toff(3,:),'r.','MarkerSize',8);
        hold on;p5 = plot3(move_Toff(1,BF_elements),move_Toff(2,BF_elements),move_Toff(3,BF_elements),'c.','MarkerSize',8);
        hold on; [elaxisp,p1,t1,p2,t2,p3,t3] =fn_axes_31Jan(axlen,axes,colour,axis_ECEF,[xs(j);ys(j);zs(j)]);
        point_sat2cell = [xs(j) xe(1);ys(j) ye(1);zs(j) ze(1)];
        hold on;pp2 = plot3(point_sat2cell(1,:),point_sat2cell(2,:),point_sat2cell(3,:),'w','Linewidth',2);
        
        if j == max_El_gnd_all_idx
            hold on;check_orientation_if_needed = plot3(move_Toff2(1,:),move_Toff2(2,:),move_Toff2(3,:),'k.','MarkerSize',8);
            delete(check_orientation_if_needed);
            zoom out;
            zoom(8)
        end
        view([xs(j);ys(j);zs(j)])
        drawnow
        frame = getframe(h);
        if save_anim == 1
            writeVideo(writerObj,frame)  %// add the frame to the movie
        end
        delete(p1);delete(p2);delete(p3);delete(t1);delete(t2);delete(t3);
        delete(p4);delete(p5);delete(h4);delete(pp2);
    end
    if bw == length(bw3filenames) && save_anim == 1,   close(writerObj);   end
    
    %
    figure;plot(mispointed_dist);xlabel('Time Elapsed (x 20s)');ylabel('Mispointed distance (km)');title("Plot of mispointed distance due to roll error");
    
    figure;box on;axis([-180 180 -90 90])
    load('topo.mat', 'topo', 'topomap1');
    topoe = [topo(:,181:360) topo(:,1:180)];
    contour(-179:180, -89:90, topoe, [0 0], 'k','HandleVisibility','off')
    for j = 1:10
        hold on;h12 = plot(elongarr(j),elatarr(j),'kx','MarkerSize',18,'HandleVisibility','off');
        hold on;t11 = text(elongarr(j)-0.05,elatarr(j),num2str(j),'FontSize',12,'HandleVisibility','off');
    end
    plot(mispointed_long,mispointed_lat,'rx','MarkerSize',8,'Displayname','Mispointed locations');
    xlabel('Longitude (\circ)');ylabel('Latitude (\circ)');title(sprintf("Mispointed locations due to roll error (towards %s)",test_site_str));
    
    test_location_lat = elatarr(1);
    test_location_long = elongarr(1);
    shifted_grid_center_lat = mispointed_lat(pass == max_El_gnd_all_idx);
    shifted_grid_center_long = mispointed_long(pass == max_El_gnd_all_idx);
    LLA1 = Ten_Beam_Placement_func(test_location_lat,test_location_long,shifted_grid_center_lat,shifted_grid_center_long,beam_alt);
    LLA2 = round(LLA1,4);
    hold on;plot(LLA1(:,2),LLA1(:,1),'b*','Displayname','Shifted grid');
    for i = 1:length(LLA1(:,1))
        text(LLA1(i,2)+0.01,LLA1(i,1)+0.01,sprintf('%d',i));
    end
    axis([min(min(elongarr),min(LLA2(:,2)))-0.2 max(max(elongarr),max(LLA2(:,2)))+0.2  min(min(elatarr),min(LLA2(:,1)))-0.2 max(max(elatarr),max(LLA2(:,1)))+0.2]);
    
    lat1 = LLA2(1,1);
    long1 = LLA2(1,2);
    lat2 = LLA2(2,1);
    long2 = LLA2(2,2);
    temp = (sin(lat1*pi/180) .* sin(lat2*pi/180)) + (cos(lat1*pi/180) .* cos(lat2*pi/180) .* cos(long2*pi/180 - long1*pi/180));
    dist_beam1_beam2 = Re * acosd(temp)*pi/180;
    title(sprintf('%s Beam Placement (distance between 1 and 2: %.2fkm)',str_name{bw},dist_beam1_beam2));xlabel('Longitude (\circ)');ylabel('Longitude (\circ)');legend
    F = getframe(gcf);imwrite(F.cdata, sprintf("%s_beams.png",str_name{bw}));
    
    figure;box on;axis([-180 180 -90 90])
    load('topo.mat', 'topo', 'topomap1');
    topoe = [topo(:,181:360) topo(:,1:180)];
    contour(-179:180, -89:90, topoe, [0 0], 'k','HandleVisibility','off')
    hold on;plot(long,lat,'Displayname','Satellite track');
    hold on;plot(elongarr(1),elatarr(1),'bx','Markersize',18,'Displayname',test_site_str);
    axis([min(long) max(long)  min(lat) max(lat)]);
    title(sprintf('%s pass (peak GE: %.2f%s)',str_name{bw},max_El_gnd_all,176));xlabel('Longitude (\circ)');ylabel('Longitude (\circ)');legend
    
    F = getframe(gcf);imwrite(F.cdata, sprintf("%s_track.png",str_name{bw}));
    
    shifted_grid = [shifted_grid LLA2];
    disp(shifted_grid)
    % clearvars -EXCEPT shifted_grid bw3filenames str_name writerObj
    % close all
    
end